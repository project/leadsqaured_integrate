<?php
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\node\Entity\Node;

/**
 * Implements hook_help().
 */
function leadsquared_integrate_help($path, $arg) {
  switch ($path) {
    // Main module help for the LeadSquared module.
    case 'admin/help#leadsquared_integrate':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The LeadSquared Integration module is a basic CRM integration to Drupal form.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<h5>' . t('In Drupal Form:') . '</h5>';
      $output .= '<p>' . t('It helps integrate Drupal form with LeadSquared CRM to capture lead. Mainly focuses on Drupal form i.e leads are captured during').'<ol>'.'<li>'. t('Registration').'</li>' .'<li>'. t('Form Submit').'</li>'.'<li>'. t('After Publishing/Payment/Update of content').'</li>'.'</ol>'. '</p>';
      $output .= '<h5>' . t('Instructions:') . '</h5>';
      $output .= '<p>' . t('After Installation you need to set LeadSquared Details i.e., 1. AccessKey 2. SecretKey 3. BaseURL ').'for those details <a href="https://in21.leadsquared.com/Settings/UserAccessKey">Login to LeadSquared. '.'</a>' . t('<br> Then you need to set LeadSquared Fields details: i.e., 1. Content Type 2. No. of fields you want to integrate and click on Add Field followed by refresh. Then you have to Feed the the machine name of the form fields & its type, corresponding to it set Leadsquared fields machine name. Note: Email, Phone are required and necessary for lead creation. So be careful and fill those machine names.') . '</p>';
      $output .= '<h5>' . t('Ways to confirm?') . '</h5>';
      $output .= '<p>' . t('1. Login into LeadSquared account for detailed list of lead pushed.<br> 2. <b>"https://{host}/v2/LeadManagement.svc/Leads.GetByEmailaddress?accessKey=Access Key&secretKey=Secret Key&emailaddress=EmailAddress"</b>  Just replace Access Key, Secret Key, EmailAddress or vist <a href="https://apidocs.leadsquared.com/get-a-lead-by-email-id/">open link</a> and click on Try it and proceed.') . '</p>';

      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function leadsquared_integrate_menu() {
  $items = array();
  $items['admin/config/crm/leadsquared/settings'] = array(
    'title' => 'Set LeadSquared',
    'description' => 'Configuration of leadsquared details',
    'page callback' => 'leadsquared_integrate_page_callback',
		'access arguments' => array('Access and change LeadSquared Details.'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/crm/leadsquared/fields'] = array(
    'title' => 'Set Fields',
    'description' => 'Configuration of leadsquared fields and form',
    'page callback' => 'leadsquared_integrate_fields_page_callback',
    'access arguments' => array('Access and change LeadSquared Fields.'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}
/**
 * Implements hook_page_callback().
*/
function leadsquared_integrate_page_callback() {
return drupal_get_form('leadsquared_integrate_form');
}
/**
 * Implements hook_page_callback().
*/
function leadsquared_integrate_fields_page_callback() {
//The argument is the name of the function with the form details
return drupal_get_form('leadsquared_integrate_fields_form');
}

/**
* Implements hook_permission
*/
function leadsquared_integrate_permission() {
    return array(
            'Access and change LeadSquared Details.' => array(
                'title' => t('Access to LeadSquared CRM'),
            ),
            'Access and change LeadSquared Fields.' => array(
                'title' => t('Access to LeadSquared Fields'),
            ),
    );
}

/**
 * Implements hook_form().
*  LeadSquared Details
*/
function leadsquared_integrate_form() {
	$form['leadsquared_form_fieldset'] = array(
		'#type' => 'fieldset',
		'#title' => 'Set LeadSquared Details: ',
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
  );
	$form['leadsquared_form_fieldset']['access-key'] = array(
    '#type' => 'textfield',
    '#title' => t('AccessKey'),
    '#default_value' => variable_get('access_key_val'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );
	$form['leadsquared_form_fieldset']['secret-key'] = array(
    '#type' => 'password',
    '#title' => t('SecretKey'),
    '#default_value' => variable_get('secret_key_val'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );
	$form['leadsquared_form_fieldset']['base-url'] = array(
    '#type' => 'textfield',
    '#title' => t('BaseURL'),
    '#default_value' => variable_get('base_url_val'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );
  $form['leadsquared_form_fieldset']['base-url']['#attributes']['placeholder'] = t('e.g., https://api-in21.leadsquared.com/v2/LeadManagement.svc');

	$form['leadsquared_form_fieldset']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Set'),
    '#submit' => array(
      'leadsquared_form_submit',
    ),
  );
  $form['leadsquared_form_fieldset']['submit']['#prefix'] = '<div class="col-md-3">';
  $form['leadsquared_form_fieldset']['submit']['#suffix'] = '</div>';

  $form['leadsquared_form_fieldset']['reset-details'] = array(
  '#type' => 'submit',
  '#value' => t('Reset'),
  '#href' => '',
  '#submit' => array(
     'custom_reset_details',
   ),
   '#prefix' => '<div class="col-md-3" id="reset">',
   '#suffix' => '</div>',
  );
  $form['leadsquared_form_fieldset']['reset-details']['#limit_validation_errors'] = array();

  global $base_url;
  $markup_content = '<div><a class="btn nextpage" style = "padding: 7px;" href="'.$base_url.'/admin/config/crm/leadsquared/fields">Set LeadSquared Fields </a></div>';
  $form['leadsquared_form_fieldset']['markup_link'] = array(
    '#markup' => variable_get('markup_link',t($markup_content)),
  );
  $form['leadsquared_form_fieldset']['markup_link']['#prefix'] = '<div class="col-md-3">';
  $form['leadsquared_form_fieldset']['markup_link']['#suffix'] = '</div>';

  $form['#attached']['css'] = array(
  drupal_get_path('module', 'leadsquared_integrate'). '/css/leadsquared_integrate.css',
  );

  $form['#validate'][] = 'leadsquared_form_validate';
  return system_settings_form($form);
}


/**
 * Implements hook_form().
*  LeadSquared Field Details
*/
function leadsquared_integrate_fields_form() {
  $form['leadsquared_fields_form_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => 'Set LeadSquared Fields Details: ',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['leadsquared_fields_form_fieldset']['content-type-id'] = array(
    '#type' => 'textfield',
    '#title' => t('Content Type :'),
    '#default_value' => variable_get('type_id'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );
  $form['leadsquared_fields_form_fieldset']['content-type-id']['#attributes']['placeholder'] = t('e.g., application_node_form');
  if(variable_get('no_of_field_val') == "") {
     variable_set('no_of_field_val',5);
  }
  $form['leadsquared_fields_form_fieldset']['total-fields'] = array(
    '#type' => 'textfield',
    '#title' => t('No. of fields you want to integrate :'),
    '#default_value' => variable_get('no_of_field_val'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  variable_set('leadsquared_field1_val', 'EmailAddress');
  variable_set('leadsquared_field2_val', 'FirstName');
  variable_set('leadsquared_field3_val', 'LastName');
  variable_set('leadsquared_field4_val', 'Phone');
  variable_set('leadsquared_field5_val', 'Mobile');

  $form['leadsquared_fields_form_fieldset']['add-fields'] = array(
  '#type' => 'submit',
  '#value' => t('Add Field'),
  '#submit' => array(
      'custom_add_fields',
    ),
   '#prefix' => '<div class="col-md-12" id="addmore">',
   '#suffix' => '</div>',
  );
  $form['leadsquared_fields_form_fieldset']['add-fields']['#limit_validation_errors'] = array();

  $maxfield = variable_get('no_of_field_val');
  if ($maxfield <= 200 && $maxfield > 0) {
    $form['leadsquared_fields_form_fieldset']['head1-mark1'] = array(
    '#markup' => variable_get('head1_markup',  t('<div class="col-md-12"><h5>Add details about the machine-name of the fields, you wish to integrate.</h5></div>')),
    );
    for ($i = 1; $i <= $maxfield; $i++) {
      $form['leadsquared_fields_form_fieldset'][$i]['field'.$i] = array(
        '#type' => 'textfield',
        '#title' => t('Field'.$i),
        '#default_value' => variable_get('field'.$i.'_val'),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
      );

      $form['leadsquared_fields_form_fieldset'][$i]['fieldtype'.$i] = array(
        '#type' => 'select',
        '#title' => t('Field Type'.$i),
        '#options' => drupal_map_assoc(array(t('TextField'), t('Email'), t('Mobile'), t('Other'))),
        '#default_value' => variable_get('fieldtype'.$i.'_val'),
        '#size' => null,
        '#maxlength' => 128,
        '#required' => FALSE,
      );
      $form['leadsquared_fields_form_fieldset'][$i]['leadsquared_field'.$i] = array(
        '#type' => 'textfield',
        '#title' => t('LeadSquared Field'.$i),
        '#default_value' => variable_get('leadsquared_field'.$i.'_val'),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
      );
    }
   }

  $form['leadsquared_fields_form_fieldset']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Set'),
    '#submit' => array(
      'leadsquared_form_fields_submit',
    ),
    '#prefix' => '<div class="col-md-4">',
    '#suffix' => '</div>',
  );
  $form['leadsquared_fields_form_fieldset']['reset-btn'] = array(
  '#type' => 'submit',
  '#value' => t('Reset'),
  '#submit' => array('custom_reset_fields'),
  '#prefix' => '<div class="col-md-6" id="reset">',
  '#suffix' => '</div>',
  );
  $form['leadsquared_fields_form_fieldset']['reset-btn']['#limit_validation_errors'] = array();

$form['#attached']['js'] = array(
  drupal_get_path('module', 'leadsquared_integrate'). '/js/leadsquared_integrate.js',
 );
$form['#attached']['css'] = array(
   drupal_get_path('module', 'leadsquared_integrate'). '/css/leadsquared_integrate.css',
 );
 $form['#validate'][] = 'leadsquared_fields_validate';
 return system_settings_form($form);
}

//Custom Functions
function custom_add_fields($form, &$form_state) {

  $maxfield = $form_state['input']['total-fields'];
  $conentypeid = $form_state['input']['content-type-id'];
  variable_set('no_of_field_val', $maxfield);
  variable_set('type_id', $conentypeid);
}

function custom_reset_fields($form, &$form_state) {
  $maxfield = variable_get('no_of_field_val');
   for ($i = 1; $i <= $maxfield; $i++) {
       variable_del('field'.$i.'_val');
       variable_del('fieldtype'.$i.'_val');
       variable_del('leadsquared_field'.$i.'_val');
    }
  variable_del('no_of_field_val');
  variable_del('type_id');
}

function custom_reset_details($form, &$form_state) {
  variable_del('access_key_val');
  variable_del('secret_key_val');
  variable_del('base_url_val');
}


/**
 * Implements hook_validate().
*/
function leadsquared_form_validate($form, &$form_state)
{
  $accessky = $form_state['input']['access-key'];
   if($accessky == ""){
      form_set_error('access-key', t('AccessKey is required'));
			return FALSE;
    }
	$secretky = $form_state['input']['secret-key'];
	   if($secretky == ""){
	      form_set_error('secret-key', t('SecretKey is required'));
				return FALSE;
	    }
	$baseurl = $form_state['input']['base-url'];
			if($baseurl == ""){
			    form_set_error('base-url', t('BaseURL is required'));
					return FALSE;
			 }


}
/**
 * Implements hook_validate().
*/
function leadsquared_fields_validate($form, &$form_state) {
  $email = $form_state['input']['field1'];
  $state = variable_get('mod_state');
  if($email == "" && $state != "reset"){
     form_set_error('field1', t('Email is required'));
     return FALSE;
   }
 $fname = $form_state['input']['field2'];
  if($fname == "" && $state != "reset"){
      form_set_error('field2', t('First Name is required'));
     return FALSE;
   }
  $phone = $form_state['input']['field4'];
  if($phone == "" && $state != "reset"){
       form_set_error('field4', t('Phone is required'));
       return FALSE;
   }
}

/**
 * Implements hook_form_submit().
*/
function leadsquared_form_fields_submit($form, &$form_state) {
$maxfield = variable_get('no_of_field_val');
 for ($i = 1; $i <= $maxfield; $i++) {
     variable_set('field'.$i.'_val', $form_state['input']['field'.$i]);
     variable_set('fieldtype'.$i.'_val', $form_state['input']['fieldtype'.$i]);
     variable_set('leadsquared_field'.$i.'_val', $form_state['input']['leadsquared_field'.$i]);
  }
  }

/**
 * Implements hook_form_submit().
*/
function leadsquared_form_submit($form, &$form_state) {
  $accessky = $form_state['input']['access-key'];
	$secretky = $form_state['input']['secret-key'];
	$baseurl = $form_state['input']['base-url'];
     variable_set('access_key_val', $accessky);
     variable_set('secret_key_val', $secretky);
     variable_set('base_url_val', $baseurl);
  }


//LeadSquared Integration Logic
class Leadsquared_Api{
	var $accessKey;
	var $secretKey;
  public function __construct() {
   $this->accessKey = variable_get('access_key_val');
   $this->secretKey = variable_get('secret_key_val');
 }

 //create_lead()
 public function create_lead($data){
   $url_base = variable_get('base_url_val');
   $url = $url_base. '/Lead.Create?accessKey=' . $this->accessKey . '&secretKey=' . $this->secretKey;
   $lead_details = array();
   foreach($data as $key => $value){
     $lead_details[] = '{"Attribute":"'.$key.'", "Value": "'.$value.'"}';
     if($key == 'EmailAddress')
     {
       $email = $value;
     }
   }
   $json_data ='['.implode(",",$lead_details).']';
   return Leadsquared_Api::lsqcurl($url,$json_data,$email);
 }

 //create_update_lead()
 public function create_update_lead($data){
   $url_base = variable_get('base_url_val');
   $url = $url_base. '/Lead.CreateOrUpdate?postUpdatedLead=false&accessKey=' . $this->accessKey . '&secretKey=' . $this->secretKey;
   $lead_details = array();
   foreach($data as $key => $value){
     $lead_details[] = '{"Attribute":"'.$key.'", "Value": "'.$value.'"}';
     if($key == 'EmailAddress')
     {
       $email = $value;
     }
   }
   $json_data ='['.implode(",",$lead_details).']';
   return Leadsquared_Api::lsqcurl($url,$json_data,$email);
 }

 //update_lead()
 public function update_lead($data,$leadId,$email){
	 $url_base = variable_get('base_url_val');
	 $url = $url_base. '/Lead.Update?accessKey=' . $this->accessKey . '&secretKey=' . $this->secretKey. '&leadId=' . $leadId;
	 $lead_details = array();
	 foreach($data as $key => $value){
		 $lead_details[] = '{"Attribute":"'.$key.'", "Value": "'.$value.'"}';
	 }
	 $json_data ='['.implode(",",$lead_details).']';
	 return Leadsquared_Api::lsqcurl($url,$json_data,$email);
 }

//get_lead_by_email()
 public function get_lead_by_email($email){
   $url_base = variable_get('base_url_val');
   $url = $url_base. '/Leads.GetByEmailaddress?accessKey=' . $this->accessKey . '&secretKey=' . $this->secretKey . '&emailaddress=' . $email ;
   return Leadsquared_Api::lsqcurlget($url);
 }

 //lsqcurl($url,$json_data)
 public function lsqcurl($url,$json_data,$email)
 {
   try{
     $curl = curl_init($url);
     curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($curl, CURLOPT_HEADER, 0);
     curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data);
     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
     curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                           'Content-Type:application/json',
                           'Content-Length:'.strlen($json_data)
                           ));
     $response = curl_exec($curl);
     $array = json_decode($response,true);
     if(is_null($array))
     {
       $array = array();
     }
     if(	array_key_exists("Status",$array))
     {
       $array_json = json_decode($response,true);
       $status = $array_json['Status'];
       if( $status == 'Success' )
       {
         watchdog('leadsquared_integrate',$response,
         $variables = array(),
         $severity = WATCHDOG_NOTICE,$link = NULL);
         db_insert('leadsquared_integrate')->fields(array('response' => $response, 'email' => $email,))->execute();
       }
       else
       {
         watchdog('leadsquared_integrate',$response,
         $variables = array(),
         $severity = WATCHDOG_ERROR,$link = NULL);
       }
     }
     else
     {
       watchdog('leadsquared_integrate',$response,
       $variables = array(),
       $severity = WATCHDOG_WARNING,$link = NULL);
     }

     return $response;
     curl_close($curl);
   } catch (Exception $ex) {
     curl_close($curl);
   }
 }

 //lsqcurlget($url,$email)
 public function lsqcurlget($url){
   try{
     $curl = curl_init($url);
     curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($curl, CURLOPT_HEADER, 0);
     curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
     $response = curl_exec($curl);

     $array = json_decode($response,true);
     if(is_null($array))
     {
       $array = array();
     }
     if(	array_key_exists("Status",$array))
     {
       $array_json = json_decode($response,true);
       $status = $array_json['Status'];
       if( $status == 'Error' )
         {
           watchdog('leadsquared_integrate', $response,
           $variables = array(),
           $severity = WATCHDOG_ERROR,$link = NULL);
         }
         else
         {
           watchdog('leadsquared_integrate', $response,
           $variables = array(),
           $severity = WATCHDOG_NOTICE,$link = NULL);
				}
     }
     else
     {
       watchdog('leadsquared_integrate',$response,
       $variables = array(),
       $severity = WATCHDOG_WARNING,$link = NULL);
     }
     return $response;
     curl_close($curl);
   } catch (Exception $ex) {
     curl_close($curl);
   }
 }
}

/**
 * Implement hook_node_insert
 */
function leadsquared_integrate_node_insert($node)
{
  $getformid = variable_get('type_id');
  $form_id = $node->type.'_node_form';
  if($form_id == $getformid) {
      custom_setting_data_push_to_leadsquared($node);
 }
}

//Data Push to LeadSquared CRM
function custom_setting_data_push_to_leadsquared($node) {
global $base_url;
$nid = $node->nid;
$maxfield = variable_get('no_of_field_val');
for ($i = 1; $i <= $maxfield; $i++) {
   $field[$i] = variable_get('field'.$i.'_val');
}

$email = $node->$field[1];
$fname = $node->$field[2];
$lname = $node->$field[3];
$phone = $node->$field[4];
$mobile = $node->$field[5];
for ($i = 6; $i <= $maxfield; $i++) {
  if(!empty($field[$i])) {
   $field[$i] =  $node->{$field[$i]};
 }
}

$data = array(variable_get('leadsquared_field1_val') => $email['und'][0]['email'],variable_get('leadsquared_field2_val') => $fname['und'][0]['value'],variable_get('leadsquared_field3_val') => $lname['und'][0]['value'],
variable_get('leadsquared_field4_val')=> $phone['und'][0]['value'], variable_get('leadsquared_field5_val')=> $mobile['und'][0]['value'], );

for ($i = 6; $i <= $maxfield; $i++) {
  if(!empty($field[$i])) {
    $type = variable_get('fieldtype'.$i.'_val');
    if($type == "Email") {
     $data[variable_get('leadsquared_field'.$i.'_val')] = $field[$i]['und'][0]['email'];
    }
    else if($type == "TextField") {
      $data[variable_get('leadsquared_field'.$i.'_val')] = $field[$i]['und'][0]['value'];
    }
    else if($type == "Mobile") {
      $data[variable_get('leadsquared_field'.$i.'_val')] = $field[$i]['und'][0]['mobile'];
    }
    else {
      drupal_set_message(t("One of the field has not been pushed to the CRM. If this problem persists contact the Administrator."), 'warning');
    }
  }
}
$l1 = new Leadsquared_Api;
$l1->create_update_lead($data);
}


/**
 * Implement hook_user_insert
 */
function leadsquared_integrate_user_insert(&$edit, $account, $category) {
	global $user;
	$email = $account->mail;
	$l2 = new Leadsquared_Api;
	$data1 = array("EmailAddress" => $email);
	$l2->create_lead($data1);
}

/**
 * Implement hook_node_update
 */
function leadsquared_integrate_node_update($node) {
	$nid = $node->nid;
  $mail = variable_get('leadsquared_field1_val');
  $email =  $node->$mail['und'][0]['email'];
	if(($node->original->status == 0)&&($node->status == 1)){
    $l3 = new Leadsquared_Api;
		$json_data = $l3->get_lead_by_email($email);
		$Array = json_decode($json_data, true);
    $lead_id = $Array[0]['ProspectID'];
		$data_to_be_update = array("ProspectStage" => "Customer");
		$l3->update_lead($data_to_be_update,$lead_id,$email);
	}
	if(($node->original->status == 1)&&($node->status == 0)){
    $l3 = new Leadsquared_Api;
		$json_data = $l3->get_lead_by_email($email);
		$Array = json_decode($json_data, true);
    $lead_id = $Array[0]['ProspectID'];
		$data_to_be_update = array("ProspectStage" => "Opportunity");
		$l3->update_lead($data_to_be_update,$lead_id,$email);
	}
}
?>
